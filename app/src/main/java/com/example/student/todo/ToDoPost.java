package com.example.student.todo;

import java.util.ArrayList;

public class ToDoPost {

    public String title;
    public String date;
    public String dueDate;
    public String homework;

    public ToDoPost(String title, String date, String dueDate, String homework){
        this.title = title;
        this.date = date;
        this.dueDate = dueDate;
        this.homework = homework;
    }


}
