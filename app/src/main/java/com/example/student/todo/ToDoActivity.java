 package com.example.student.todo;

import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

 public class ToDoActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<ToDoPost> ToDoPosts = new ArrayList<ToDoPost>();

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();
    }

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_ToDo;
    }

}
